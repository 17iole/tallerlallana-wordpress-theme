<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="<?php echo home_url()?>/wp-content/themes/tallerlallana/css/style.css">
        <!--[if lt IE 9]><script src="js/vendor/selectivizr-min.js"></script><![endif]-->
        <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>

    </head>
    <body>
        <!--[if lt IE 9]>
            <div class="chromeframe"><p>You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true" target="_blank">activate Google Chrome Frame</a> to improve your experience.</p></div>
        <![endif]-->
        
        <header id="header">
		<div>
		
			<a href="<?php echo home_url(); ?>" class="logo"><img src="<?php echo home_url(); ?>/wp-content/themes/tallerlallana/img/logotip2.png" alt="eloi logo"></a>
		
			<?php get_search_form( $echo ); ?>
		</div>
	</header>
		
<?php include 'nav.php'; ?>
		
	<div id="main">
		<section id="posts">
			
			
			
			<?php
global $query_string;

$query_args = explode("&", $query_string);
$search_query = array();

foreach($query_args as $key => $string) {
	$query_split = explode("=", $string);
	$search_query[$query_split[0]] = urldecode($query_split[1]);
} // foreach

$search = new WP_Query($search_query);
?>
			

			
			
		</section>
	<?php include 'aside.php'; ?>
	</div>
	<footer>
		<div class="footer_mig">
	<div><a href="#"><?php bloginfo('name'); ?> </a> 2013 all rights reserved </div>
	
	<div>Taller la llana, classes de mitja i ganxet</div>
	<div>tallerlallana.cat | tallerlallana@gmail.com | 93 865 43 46</div><hr><div>web design: <a href="mailto:eloi.casamayor@gmail.com">Eloi Casamayor Esteve</a><br>
	</div></div>
	</footer>
        
        
        
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.8.3.min.js"><\/script>')</script>
        <script src="js/vendor/jquery.placeholder.min.js"></script>
        <script>
            $(function(){
                $('input, textarea').placeholder();
                $('.chromeframe').on('click', function() {
                    $(this).slideUp('fast');
                });
            });
        </script>
    </body>
</html>