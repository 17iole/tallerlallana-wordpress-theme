<?php
if ( function_exists('register_sidebar') )
    register_sidebar();
?>
<?php add_theme_support('post-thumbnails');
//sets the dimension of the post thumbnails (crops the image)
set_post_thumbnail_size(500, 500, true);
function new_excerpt_length($length) {
return 80;
}
add_filter('excerpt_length', 'new_excerpt_length');