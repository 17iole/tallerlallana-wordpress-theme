<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="wp-content/themes/tallerlallana/css/style.css">
        <!--[if lt IE 9]><script src="js/vendor/selectivizr-min.js"></script><![endif]-->
        <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>

    </head>
    <body>
        <!--[if lt IE 9]>
            <div class="chromeframe"><p>You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true" target="_blank">activate Google Chrome Frame</a> to improve your experience.</p></div>
        <![endif]-->
        
        <header id="header">
		<div>
		
			<a href="<?php echo site_url(); ?>" class="logo"><img src="wp-content/themes/tallerlallana/img/logotip2.png" alt="eloi logo"></a>
		
			<?php get_search_form( $echo ); ?>
		</div>
	</header>
		
	<?php get_template_part('nav'); ?>
		
	<div id="main">
		<section id="posts">
			
<?php while(have_posts()):the_post();?>
		<div class="post_sencer" onmouseout="rollover(<?php the_ID(); ?>)" onmouseover="rollover(<?php the_ID(); ?>)">
		<div class="columnafixa">
		<div class="columnaesquerra">
			<span class="infopost"><?php the_date();?></span>
			<span class="infopost">categoria: <?php the_category(''); ?></span>
			<span class="infopost"><?php $comments_count = wp_count_comments($post->ID);
			echo " $comments_count->approved ";?> comentaris<br></span>
		</div>
		<div class="columnaesquerra">
		Comparteix 
		<a class="acompartir" target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo the_permalink();?>">facebook</a>
		<a class="acompartir" target="_blank" href="http://twitter.com/home?status=<?php the_title(); echo " "; echo the_permalink(); echo " via @tallerlallana"?>">twitter</a>
		<a class="acompartir" href="https://plus.google.com/share?url={<?php echo the_permalink();?>}" onclick="javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">Google+</a>
		<?php $postid = get_the_ID(); ?> 
		<a class="acompartir" target="_blank" href="//pinterest.com/pin/create/button/?url=<?php echo the_permalink();?>&media=<?php wp_get_attachment_url( $postid ); ?>&description=<?php the_title();?>" data-pin-do="buttonPin" data-pin-config="none">Pinterest</a>
  
		</div></div>
		<div class="cos">
			<h1><?php the_title(); ?></h1><p><?php the_content(); ?></p>
			<?php comments_template( '', true ); ?>
		</div>
		<?php endwhile;?>


			

		</section>
	<?php get_template_part('aside'); ?>
	</div>
	<?php get_template_part('footer'); ?>
        
        
        
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.8.3.min.js"><\/script>')</script>
        <script src="js/vendor/jquery.placeholder.min.js"></script>
        <script>
            $(function(){
                $('input, textarea').placeholder();
                $('.chromeframe').on('click', function() {
                    $(this).slideUp('fast');
                });
            });
        </script>
    </body>
</html>