<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="<?php echo home_url()?>/wp-content/themes/tallerlallana/css/style.css">
        <!--[if lt IE 9]><script src="js/vendor/selectivizr-min.js"></script><![endif]-->
        <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
	<script>/*
	function veuretot(id){
	//alert ("veure tot el post "+id);
	var classe
		classe = document.getElementById("veuretot"+id).className
		if(classe=="veuretot"){ document.getElementById("veuretot"+id).className="veuretotsi";
		}else {document.getElementById("veuretot"+id).className="veuretot";}
		
	}
	function rollover (id){
	var classe
		classe = document.getElementById("veuretot"+id).className
		if(classe == "veuretotno"){ document.getElementById("veuretot"+id).className="veuretot";
		}else if(classe=="veuretot"){
			document.getElementById("veuretot"+id).className="veuretotno";
			}else{}
	}*/
	</script>
    </head>
    <body>
        <!--[if lt IE 9]>
            <div class="chromeframe"><p>You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true" target="_blank">activate Google Chrome Frame</a> to improve your experience.</p></div>
        <![endif]-->
        
        <header id="header">
		<div>
		
			<a href="<?php echo home_url(); ?>" class="logo"><img src="<?php echo home_url(); ?>/wp-content/themes/tallerlallana/img/logotip6.png" alt="eloi logo"></a>
		<?php get_search_form( $echo ); ?>
			
		</div>
	</header>
	
	<?php get_template_part('nav'); ?>
	
		
	<div id="main">
		<section id="posts">
		<h2>Posts trobats amb la clau  <span id="paraulaclau"><?php the_search_query();?></span>:</h2>
	<?php if ( have_posts() ) : ?>	
		<?php while ( have_posts() ) : the_post(); ?>
		
		<div class="post" onmouseout="rollover(<?php the_ID(); ?>)" onmouseover="rollover(<?php the_ID(); ?>)">
		<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
			<div>
			<header>
					
					<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
					<?php the_excerpt(); ?>
					<p class="date"><?php the_date();?></p>
					<a class="llegirmes" href="<?php the_permalink(); ?>"> llegir m&eacute;s &rarr;</a>
									
			</header>
			<div class="veuretotno" id="veuretot<?php the_ID(); ?>" onClick="veuretot(<?php the_ID(); ?> )">veure tot</div>
			</div>
		</div>
		<?php endwhile;?>
	<?php else : ?>
				<div id="post-0" class="post no-results not-found">
					<h2 class="entry-title">No hi ha resutats </h2>
					<div class="entry-content">
						<p>Ho sentim, per&#224; la teva cerca no ha obtingut resultats. Prova amb diferents paraules. </p>
						<?php get_search_form( $echo ); ?>
					</div><!-- .entry-content -->
				</div><!-- #post-0 -->
<?php endif; ?>

		<?php //wp_pagenavi( array( 'query' => $my_query ) ); 
		
		//wp_reset_postdata();	// avoid errors further down the page?>
			

		</section>

	<?php get_template_part('aside'); ?>
	</div>
	<?php get_template_part('footer'); ?>
        
        
        
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.8.3.min.js"><\/script>')</script>
        <script src="js/vendor/jquery.placeholder.min.js"></script>
        <script>
            $(function(){
                $('input, textarea').placeholder();
                $('.chromeframe').on('click', function() {
                    $(this).slideUp('fast');
                });
            });
        </script>
    </body>
</html>
